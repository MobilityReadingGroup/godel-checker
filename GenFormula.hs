module GenFormula where

import Mcrl
import GoParser
import Data.List as L
import Data.Maybe
import qualified Data.Map as M

  

-- chans should be the list of Sync_ and Closing_ actions
genTaus :: [String] -> String
genTaus [] = "tau + tausync"
genTaus chans@(x:xs) = (intercalate " + " chans)++" + tau + tausync"


-- OP_a in A <a>phi
allTauDiamond :: String -> [String] -> String -> String
allTauDiamond op [] fora = "<tau>"++fora++" "++op++" " ++"<tausync>"++fora
allTauDiamond op taus fora = intercalate (" "++op++" ") $ map (\x -> "<"++x++">"++fora) ("tau":"tausync":taus)

-- OP_a in A [a]phi
allTauBox :: String -> [String] -> String -> String
allTauBox op [] fora = "[tau]"++fora++" "++op++" " ++"[tausync]"++fora
allTauBox op taus fora = intercalate (" "++op++" ") $ map (\x -> "["++x++"]"++fora) ("tau":"tausync":taus)


eventuallyHolds :: [String] -> String -> String
eventuallyHolds taus fora = 
  "mu Z .("++(fora)++") || ("++(allTauDiamond "||" taus "Z")++")"

noTerminalState :: [String] -> String
noTerminalState chans = "[("++(genTaus chans)++")*]<"++(genTaus chans)++">true"

noCycle :: [String] -> String
noCycle chans = "[("++(genTaus chans)++")*](mu Y . ["++(genTaus chans)++"]Y)"

globalDeadlock :: [String] -> [BarbContent] -> String
globalDeadlock [] _ = "true % no channel"
globalDeadlock chans@(x:xs) barbs =
  if genAllBarbs == ""
  then "true % no barbs"
  else "[("++(genTaus chans)++")*]\n( <"++(genAllBarbs)++">true\n=>\n<"++(genTaus chans)++">true)"
  where
    genAllBarbs = intercalate " + " $ map printBarbs $
                  filter isSndRcv barbs
    isSndRcv (Closed s) = False
    isSndRcv (Closing s) = False
    isSndRcv (NonEmpty s) = False
    isSndRcv t = True                 
                                                         


isInBarb :: String -> BarbContent -> Bool
isInBarb s (Send t) = s == t
isInBarb s (Rcv t) = s == t
isInBarb s (Sel xs) = or $ map (isInBarb s) xs
isInBarb s t = False

oneLiveness :: Maybe String -> [String] -> BarbContent -> Maybe String
oneLiveness chan taus b = case chan of
  Nothing -> fun b
  Just c -> if isInBarb c b then fun b else Nothing
  where fun b = case b of
          (Send s) -> Just $ sendrcv b s
          (Rcv s) -> Just $ sendrcv b s
          (Sel xs@(y:ts)) ->  Just $ "(<"++(printBarbs b)++">true => "
                              ++(eventuallyHolds taus (helper xs))++")"
          t -> Nothing
  
        sendrcv t chan =  "(<"++(printBarbs t)++">true => "
                          ++(eventuallyHolds taus ("<Sync_"++chan++">true"))++")"
        printChan (Send s) = "(<Sync_"++s++">true)"
        printChan (Rcv s) = "(<Sync_"++s++">true)"
        helper xs = intercalate " || " $ map printChan xs


liveness :: Maybe String -> [String] -> [BarbContent] -> String
liveness chan taus xs =
  let list = catMaybes $ map (oneLiveness chan taus) xs
  in case list of
    [] -> "true % nothing to check here"
    ys -> "[("++(genTaus taus)++")*]\n(\n"
          ++(intercalate "\n&&\n" list)++"\n)"




oneSafety :: [BarbContent] -> BarbContent ->  Maybe String
oneSafety barbs b@(Closed s) = baddies >>= (\x ->  return $ "<"++(printBarbs b)++">true => "++x)
  where baddies = case filter (`elem` [Closing s]) barbs of
          [] -> Nothing
          xs -> Just $ "[("++(intercalate " + " $
                              map printBarbs $ filter (`elem` [Send s, Closing s]) barbs)++")]false"
oneSafety _ t = Nothing




safety :: [String] -> [BarbContent] -> Maybe String
safety taus xs = case catMaybes $ map (oneSafety xs) xs of
  [] -> Nothing -- "true % not closing channel primitive"
  list -> Just $ "[("++(genTaus taus)++")*]\n(\n"++(intercalate "\n&&\n" list)++"\n)"





oneEventualReception :: [String] -> BarbContent -> Maybe String
oneEventualReception taus b =  fun b
  where fun b = case b of
          (NonEmpty s) -> Just $ sendrcv b s
          t -> Nothing
        sendrcv t chan =  "(<"++(printBarbs t)++">true => mu X . <"
                          ++(genTaus taus)++">X || <Sync_"++chan++">true)"
  


eventualReception :: [String] -> [BarbContent] -> String
eventualReception taus xs =
  let list = catMaybes $ map (oneEventualReception taus) xs
  in case list of
    [] -> "true % nothing to check here"
    ys -> "[("++(genTaus taus)++")*]\n(\n"
          ++(intercalate "\n&&\n" list)++"\n)"
