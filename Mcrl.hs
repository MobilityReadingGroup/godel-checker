module Mcrl where

import Data.List as L
import qualified Data.Map as M
import Data.Char (toUpper)

data BarbContent = Sel [BarbContent] -- NB: xs in Sel[xs] is assumed to contain Send/Rcv only
                 | Rcv String
                 | Send String
                 | Closed String
                 | Closing String
                 | NonEmpty String
                 deriving (Eq, Show, Ord)

data Synchro = End String
             | Start String
             deriving (Eq, Show, Ord)

data Action = Barb BarbContent
            | Action String String
            | Request String
            deriving (Eq, Show, Ord)

data Model = Dot Model Model
           | Plus Model Model
           | A Action
           | Tau
           | Delta
           | Par Model Model
           | Rec String
           | Choice [(String, Model)]
           deriving (Eq, Show, Ord)



printBarbs :: BarbContent -> String
printBarbs s = case s of
  (Sel xs) -> helper xs
  (Rcv s) -> "RECV_"++s
  (Send s) -> "SEND_"++s
  (Closing s) -> "CHANCLOSING_"++s
  (Closed s) -> "CHANCLOSED_"++s
  (NonEmpty s) -> "NONEMPTY_"++s
  where helper xs = foldl (++) "SELECT_" $ map
                    (\x -> case x of
                        (Rcv chan) -> "R_"++chan
                        (Send chan) -> "S_"++chan
                    ) xs


-- (tau . delta + tau . delta) . (stuff) IS BAD, but this shouldn't happen anymore

printModel :: Model -> String
printModel t = fun t
  where fun (Dot m1 m2) = "("++(fun m1)++") . ("++(fun m2)++")"
        fun (Plus m1 m2) = "("++(fun m1)++") + ("++(fun m2)++")"
        fun (A (Barb s)) = printBarbs s
        fun (A (Action ty chan)) = ty++chan
        fun (A (Request s)) = "REQ_"++s
        fun Tau = "tau"
        fun Delta = "delta"
        fun (Par m1 m2) = "("++(fun m1)++")\n|| ("++(fun m2)++")"
        fun (Rec s) = s
        fun (Choice xs) = intercalate " + " $ map helper xs
        helper (g,bod)
          | null g = (fun bod)
          | otherwise = "\n("++g++") -> "++(fun bod)



getActions :: Model -> [String]
getActions t = fun t
  where fun (A (Action ty chan)) = [ty++chan]
        fun (Par m1 m2) = (fun m1)++(fun m2)
        fun (Dot m1 m2) = (fun m1)++(fun m2)
        fun (Plus m1 m2) = (fun m1)++(fun m2)
        fun (Choice xs) = foldr (++) [] $ map (fun . snd) xs
        fun t = []

        
getGuards :: Model -> [BarbContent]
getGuards t = fun t
  where fun (A (Barb s)) = [s]
        fun (Par m1 m2) = (fun m1)++(fun m2)
        fun (Dot m1 m2) = (fun m1)++(fun m2)
        fun (Plus m1 m2) = (fun m1)++(fun m2)
        fun (Choice xs) = foldr (++) [] $ map (fun . snd) xs
        fun t = []



