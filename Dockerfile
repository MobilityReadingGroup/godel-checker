FROM debian:jessie-slim as kittel
RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
    wget cmake git make ca-certificates ssh-client \
    clang llvm-dev mcrl2 ocaml-nox camlp4 \
    libocamlgraph-ocaml-dev camlidl libapron-ocaml-dev \
    libedit-dev zlib1g-dev libgmp-dev \
    ghc cabal-install && \
    cd /usr/local/src && \
    git clone https://github.com/s-falke/llvm2kittel && \
    git clone https://github.com/s-falke/kittel-koat && \
    mkdir build && cd build && cmake ../llvm2kittel && \
    make && mv llvm2kittel /usr/bin && \
    cd ../kittel-koat && git checkout 6ee36da && \
    HAVE_Z3=false make kittel && mv /usr/local/src/kittel-koat/_build/kittel.native /usr/bin/ && \
    cd .. && rm -rf kittel-koat && rm -rf llvm2kittel && rm -rf build && \
    wget -O yices-1.0.40-x86_64-unknown-linux-gnu-static-gmp.tar.gz \
      'http://yices.csl.sri.com/cgi-bin/yices-newdownload.cgi?file=yices-1.0.40-x86_64-unknown-linux-gnu-static-gmp.tar.gz&accept=I%20accept' && \
    tar zxf yices-1.0.40-x86_64-unknown-linux-gnu-static-gmp.tar.gz && \
    mv yices-1.0.40/include/* /usr/include/ && \
    mv yices-1.0.40/bin/* /usr/bin/ && \
    mv yices-1.0.40/lib/* /usr/lib/ && \
    rm -rf yices-1.0.40* && \
    apt-get remove -y wget camlp4 ocaml-nox cmake --purge &&\
    apt-get autoremove -y --purge&& \
    rm -rf /var/lib/apt/lists/*

FROM haskell:8 as godel
RUN apt-get update -y && \
    apt-get install -y --no-install-recommends git ca-certificates ssh-client && \
    cd /usr/local/src && \
    git clone https://bitbucket.org/MobilityReadingGroup/godel-checker.git && \
    cd godel-checker && \
    cabal update && cabal install cmdargs parsec ansi-terminal && \
    ghc Godel.hs && \
    apt-get remove -y git ghc cabal-install && \
    apt-get autoremove -y --purge && \
    rm -rf /var/lib/apt/lists/*

FROM debian:jessie-slim
MAINTAINER Nicholas Ng <nickng@nickng.io>
WORKDIR /root
RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
      libgmp-dev zlib1g libmpfr4 mcrl2 clang
COPY --from=kittel /usr/bin/yices /usr/bin
COPY --from=kittel /usr/include/yices_c.h /usr/include
COPY --from=kittel /usr/include/yicesl_c.h /usr/include
COPY --from=kittel /usr/lib/libyices.a /usr/lib
COPY --from=kittel /usr/bin/llvm2kittel /usr/bin
COPY --from=kittel /usr/bin/kittel.native /usr/bin
COPY --from=godel /usr/local/src/godel-checker/Godel /usr/bin
RUN apt-get autoremove -y --purge && rm -rf /var/lib/apt/lists/*
